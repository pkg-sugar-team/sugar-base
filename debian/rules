#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2007-2012, 2015-2017 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for sugar-base
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEB_BUILDDIR = build
include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/python-autotools.mk

pkg = python-sugar
pkgbranch = $(basename $(DEB_UPSTREAM_VERSION))

# Needed by upstream build process
#    (pygtk needed by autotools but seems bogus: no linkage in binaries)
CDBS_BUILD_DEPENDS +=, python-gtk2-dev, intltool
CDBS_BUILD_DEPENDS +=, python-gobject-2-dev | python-gobject-dev

# Needed (always/often/seldom) at runtime
#  * ipython beautifies tracebacks in logs
CDBS_DEPENDS_DEFAULT = python-decorator
CDBS_DEPENDS_DEFAULT +=, python-gobject-2 | python-gobject
CDBS_SUGGESTS_DEFAULT = ipython

# Remove .la file
binary-post-install/$(pkg)::
	find $(DEB_DESTDIR)/usr -type f -name '*.la' -delete

# Strip hardcoded rpath
CDBS_BUILD_DEPENDS += , chrpath
binary-strip/$(pkg)::
	find $(DEB_DESTDIR)/usr/lib -type f -name '*.so' -exec chrpath -d '{}' ';'

# Fix favor ISO 639-1 when differing from ISO 639-2
binary-post-install/$(pkg)::
	mv debian/$(cdbs_curpkg)/usr/share/locale/aym \
		debian/$(cdbs_curpkg)/usr/share/locale/ay

# Cleanup a few empty subdirs missed by py-compile.mk cleanup routines
#  * TODO: drop cdbs_python_curpkg_build_versions fallback when cdbs 0.5 is stable
clean::
	-[ ! -d $(DEB_BUILDDIR) ] || for flavor in $(or $(cdbs_$python_allflavors),$(cdbs_python_curpkg_build_versions)); do \
		(cd $(DEB_BUILDDIR) && rmdir -p $$flavor/src/sugar); \
	done
	-[ ! -d $(DEB_BUILDDIR) ] || rmdir $(DEB_BUILDDIR)
